package com.atlassian.atlascamp;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.DefaultImagePlaceholder;
import com.atlassian.confluence.macro.EditorImagePlaceholder;
import com.atlassian.confluence.macro.ImagePlaceholder;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.thumbnail.Dimensions;
import com.atlassian.confluence.setup.settings.SettingsManager;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class NewCheeseMacro implements Macro, EditorImagePlaceholder {
    private static final Dimensions DIMS = new Dimensions(200, 200);

    private static final String RESOURE_DIR = "/download/resources/com.atlassian.atlascamp.atlascamp2013/";
    private static final String PARAM_NAME = "type";

    private static final Map<String, String> IMAGES = new HashMap<String, String>();

    static {
        IMAGES.put("Mouldy", "mouldy.jpg");
        IMAGES.put("Old", "old.jpg");
        IMAGES.put("Brieber", "bieber.jpg");
        IMAGES.put("Camembert", "camembert.jpg");
        IMAGES.put("Jarlsberg", "jarlsberg.jpg");
        IMAGES.put("Pauly Shore", "pauly.jpg");
    }

    private final SettingsManager settingsManager;

    public NewCheeseMacro(@NotNull final SettingsManager settingsManager) {
        this.settingsManager = settingsManager;
    }

    public String getImageLocation(@NotNull final Map<String, String> params, @NotNull final ConversionContext ctx) {
        if (params.containsKey(PARAM_NAME) && IMAGES.containsKey(params.get(PARAM_NAME))) {
            return RESOURE_DIR + IMAGES.get(params.get(PARAM_NAME));
        }
        return "";
    }

    @Override
    public ImagePlaceholder getImagePlaceholder(@NotNull final Map<String, String> parameters, @NotNull final ConversionContext context) {
        return new DefaultImagePlaceholder(getImageLocation(parameters, context), DIMS, false);
    }

    @Override
    public String execute(@NotNull final Map<String, String> parameters,
                          @NotNull final String body,
                          @NotNull final ConversionContext context)
            throws MacroExecutionException {
        if (parameters.containsKey(PARAM_NAME) && StringUtils.isNotBlank(parameters.get(PARAM_NAME))) {
            return "<img src=\"" + settingsManager.getGlobalSettings().getBaseUrl() + "/" + getImageLocation(parameters, context) + "\">";
        }
        return "";
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
